import { Effect, Actions } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import * as AuthActions from '../store/auth.actions';
import * as firebase from "firebase";
import { fromPromise } from "rxjs/observable/fromPromise";
import { Router } from '@angular/router';
import 'rxjs-compat/operator/do';
import 'rxjs-compat/operator/mergeMap';

@Injectable()
export class AuthEffects {
    @Effect()
    authSignup = this.actions$
                        .ofType(AuthActions.START_SIGNUP)
                        .map((action: AuthActions.StartSignup) => {
                            return action.payload;
                        })
                        .switchMap((userdata: {username: string, password: string}) => {
                            return firebase.auth().createUserWithEmailAndPassword(userdata.username, userdata.password);
                        })
                        .switchMap(() => {
                            return fromPromise(firebase.auth().currentUser.getIdToken());
                        })
                        .mergeMap((token: string) => {
                            return [
                                {
                                    type: AuthActions.SIGNUP
                                },
                                {
                                    type: AuthActions.SET_TOKEN,
                                    payload: token
                                }
                            ]
                        });
        ;

    @Effect()
    authSignin = this.actions$
                        .ofType(AuthActions.START_SIGNIN)
                        .map((action: AuthActions.StartSignin) => {
                            return action.payload
                        })
                        .switchMap((userdata: {username: string, password: string}) => {
                            return firebase.auth().signInWithEmailAndPassword(userdata.username, userdata.password);
                        })
                        .switchMap(() => {
                            this.router.navigate(["/"]);
                            return fromPromise(firebase.auth().currentUser.getIdToken());
                        })
                        .mergeMap((token: string) => {
                            return [
                                {
                                    type: AuthActions.SIGNIN
                                },
                                {
                                    type: AuthActions.SET_TOKEN,
                                    payload: token
                                }
                            ]
                        });
                        
    @Effect({dispatch: false})
    authLogout = this.actions$
                        .ofType(AuthActions.LOGOUT)
                        .do(() => {
                            this.router.navigate(['/']);
                        })


    constructor(private actions$: Actions, private router: Router) {
        
    }
}