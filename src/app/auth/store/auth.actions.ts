import { Action } from "@ngrx/store";

export const START_SIGNIN = 'START_SIGNING';
export const SIGNIN = 'SIGNING';
export const START_SIGNUP = 'START_SIGNUP';
export const SIGNUP = 'SIGNUP';
export const LOGOUT = 'LOGOUT';
export const SET_TOKEN = 'SET_TOKEN';

export class StartSignin implements Action {
    readonly type = START_SIGNIN;
    constructor(public payload: {username: string, password: string}) {}
}

export class Signin implements Action {
    readonly type = SIGNIN;
}

export class StartSignup implements Action {
    readonly type = START_SIGNUP;
    constructor(public payload: {username: string, password: string}) {}
}
export class Signup implements Action {
    readonly type = SIGNUP;
}

export class Logout implements Action {
    readonly type = LOGOUT;
}

export class SetToken implements Action {
    readonly type = SET_TOKEN;
    constructor(public payload: string) {}
}

export type AuthActions = Signin | Signup | Logout | SetToken | StartSignup;