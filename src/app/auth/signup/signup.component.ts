import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AppState } from 'src/app/store/app.reducers';
import { Store } from '@ngrx/store';
import { StartSignup } from '../store/auth.actions';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.store.dispatch(new StartSignup({username: form.value.email, password: form.value.password}));
  }

}
