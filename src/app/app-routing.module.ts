import { NgModule } from "@angular/core";
import { Routes, RouterModule, PreloadAllModules } from "@angular/router";
import { ShoppingListComponent } from "./shopping-list/shopping-list.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { HomeComponent } from "./core/home/home.component";


const appRoutes: Routes = [
  { path: "", component: HomeComponent, pathMatch: "full" },
  { path: "shopping-list", component: ShoppingListComponent },
  { path: "recipes", loadChildren: './recipes/recipes.module#RecipesModule' },
  { path: "not-found", component: NotFoundComponent },
  { path: "**", redirectTo: "not-found" }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {}
