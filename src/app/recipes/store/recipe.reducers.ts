import { Recipe } from "../recipe.model";
import * as RecipeActions from './recipe.actions';
import { AppState } from "src/app/store/app.reducers";

export interface FeatureState extends AppState {
    recipes: RecipeState
}

export interface RecipeState {
    recipes: Recipe[]
  }
  
  const initialState: RecipeState = {
    recipes: []
  };

export function recipeReducers(state = initialState, action: RecipeActions.RecipeActions) {
  switch(action.type) {
    case RecipeActions.SET_RECIPES:
      return {
        ...state,
        recipes: action.payload
      }
    case RecipeActions.ADD_RECIPE:
      return {
        ...state,
        recipes: [...state.recipes, action.payload]
      }
    case RecipeActions.UPDATE_RECIPE:
      const oldRecipe = state.recipes[action.payload.index];
      const updatedRecipe = {...oldRecipe, ...action.payload.updatedRecipe}
      state.recipes[action.payload.index] = updatedRecipe;
      return {
        ...state
      }
    case RecipeActions.DELETE_RECIPE:
      const clonedRecipes = [...state.recipes];
      clonedRecipes.splice(action.payload, 1);
      return {
        ...state,
        recipes: [...clonedRecipes]
      }
    default:
      return state;
  }

}