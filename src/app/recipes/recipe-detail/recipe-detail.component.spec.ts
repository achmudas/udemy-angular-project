import { TestBed, async } from '@angular/core/testing';
import { RecipeDetailComponent } from './recipe-detail.component';
import { StoreModule, Store } from '@ngrx/store';
import { Recipe } from '../recipe.model';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { SetRecipes } from '../store/recipe.actions';
import { recipeReducers } from '../store/recipe.reducers';
import { reducers } from '../../store/app.reducers';
import { BehaviorSubject } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

export class MockActivatedRoute {
  private paramsSubject = new BehaviorSubject(this.testParams);
  private _testParams: {};
  snapshot = {};

  params = this.paramsSubject.asObservable();

  get testParams() {
      return this._testParams;
  }
  set testParams(newParams: any) {
      this._testParams = newParams;
      this.paramsSubject.next(newParams);
  }
}

describe('Component recipe-detail', () => {
  let activeRoute: MockActivatedRoute;
  beforeEach(() => {
    activeRoute = new MockActivatedRoute();
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        StoreModule.forRoot(reducers), StoreModule.forFeature('recipes', recipeReducers)],
      declarations: [RecipeDetailComponent],
      providers: [{ provide: ActivatedRoute, useValue: activeRoute }],
    });
  });

  it('should create the recipe-detail', () => {
    const fixture = TestBed.createComponent(RecipeDetailComponent);
    const recipeDetail = fixture.debugElement.componentInstance;
    expect(recipeDetail).toBeTruthy();
  });

  it('should display recipe details', async(() => {
    const fixture = TestBed.createComponent(RecipeDetailComponent);
    const store = fixture.debugElement.injector.get(Store);
    store.dispatch(new SetRecipes([new Recipe(0, 'SomeRecipe', 'SomeDescr', 'image_url', [new Ingredient('Meat', 1)])]));
    activeRoute.testParams = {'id': 0};
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toBe('SomeRecipe');
  }));
});
