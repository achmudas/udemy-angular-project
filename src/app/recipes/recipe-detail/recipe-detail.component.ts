import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router, Params } from "@angular/router";
import { Store } from "@ngrx/store";
import * as ShoppingListActions from "../../shopping-list/store/shopping-list.actions";
import { RecipeState, FeatureState } from "../store/recipe.reducers";
import { DeleteRecipe } from "../store/recipe.actions";
import { Recipe } from "../recipe.model";
import "rxjs/add/operator/take";

@Component({
  selector: "app-recipe-detail",
  templateUrl: "./recipe-detail.component.html",
  styleUrls: ["./recipe-detail.component.css"]
})
export class RecipeDetailComponent implements OnInit {
  private id: number;
  recipe: Recipe;

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private store: Store<FeatureState>
  ) {}

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = +params["id"];
      this.store
        .select("recipes")
        .take(1)
        .subscribe((recipeState: RecipeState) => {
          this.recipe = recipeState.recipes[this.id];
        });
    });
  }

  addIngredientsToShoppingList() {
    this.store
      .select("recipes")
      .take(1)
      .subscribe((recipesState: RecipeState) => {
        this.store.dispatch(
          new ShoppingListActions.AddIngredients(
            recipesState.recipes[this.id].ingredients
          )
        );
      });
  }

  onDeleteRecipe() {
    this.store.dispatch(new DeleteRecipe(this.id));
    this.router.navigate(["recipes"]);
  }
}
