import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Ingredient } from "../../shared/ingredient.model";
import { NgForm } from '@angular/forms';
import { Subscription } from "rxjs";
import { Store } from "@ngrx/store";
import { AddIngredient, DeleteIngredient, UpdateIngredient, StopEdit } from '../store/shopping-list.actions';
import { AppState } from "src/app/store/app.reducers";

@Component({
  selector: "app-shopping-edit",
  templateUrl: "./shopping-edit.component.html",
  styleUrls: ["./shopping-edit.component.css"]
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

  @ViewChild('form') form: NgForm;
  subscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingredient;

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.subscription = this.store.select('shoppingList').subscribe(data => {
      if (data.editedIngredientIndex > -1) {
        this.editedItemIndex = data.editedIngredientIndex;
        this.editedItem = data.editedIngredient;
        this.editMode = true;
        this.form.setValue({
          name: this.editedItem.name,
          amount: this.editedItem.amount
        });
      } else {
        this.editMode = false;
      }
    });
  }

  ngOnDestroy() {
    this.store.dispatch(new StopEdit());
    this.subscription.unsubscribe();
  }

  onSubmit() {
    const item = this.form.value;
    const newIngredient =  new Ingredient( item.name, item.amount);
    if (this.editMode) {
      this.store.dispatch(new UpdateIngredient(newIngredient));
    } else {
      this.store.dispatch(new AddIngredient(newIngredient));
    }
    this.onClear();
  }

  onClear() {
    this.form.reset();
    this.editMode = false;
  }

  onDelete() {
    this.store.dispatch(new DeleteIngredient());
    this.onClear();
  }
}
