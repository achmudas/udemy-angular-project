import { Component, OnInit } from "@angular/core";
import { BackendService } from "../../shared/backend.service";
import { Recipe } from "../../recipes/recipe.model";
import { AuthState } from "src/app/auth/store/auth.reducers";
import { Observable } from "rxjs";
import { Store } from "@ngrx/store";
import { Logout } from "src/app/auth/store/auth.actions";
import {
  FeatureState,
  RecipeState
} from "src/app/recipes/store/recipe.reducers";
import { SetRecipes } from "src/app/recipes/store/recipe.actions";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit {
  constructor(
    private backendService: BackendService,
    private store: Store<FeatureState>
  ) {}

  authState: Observable<AuthState>;

  ngOnInit() {
    this.authState = this.store.select("auth");
  }

  onSaveData() {
    this.store
      .select("recipes")
      .take(1)
      .subscribe((recipeState: RecipeState) => {
        this.backendService
          .saveRecipes(recipeState.recipes)
          .subscribe(
            response => console.log("Recipes saved", response),
            err => console.log("Error", err)
          );
      });
  }

  onFetchData() {
    this.backendService
      .getRecipes()
      .subscribe(
        (recipes: Recipe[]) => this.store.dispatch(new SetRecipes(recipes)),
        err => console.log("Error", err)
      );
  }

  onLogout() {
    this.store.dispatch(new Logout());
  }
}
