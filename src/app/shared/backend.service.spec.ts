import { BackendService } from "./backend.service";
import { Recipe } from "../recipes/recipe.model";
import { Ingredient } from "./ingredient.model";
import { of } from "rxjs";

let httpClientSpy: { get: jasmine.Spy, put: jasmine.Spy };
let backendService: BackendService;

describe("Backend service", () => {
  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put']);
    backendService = new BackendService(<any>httpClientSpy);
  });

  it("should retrieve recipes", () => {
    const expectedRecipes: Recipe[] = [
      new Recipe(0, 'TestRecipe', 'DescriptionTest', 'http://image.url', [
        new Ingredient("testing", 2),
        new Ingredient("TestBread", 4)
      ]),
      new Recipe(1, 'TestRecipe2', 'DescriptionTest2', 'http://image2.url', [
        new Ingredient("TestBread", 4)
      ])
    ];

    httpClientSpy.get.and.returnValue(of(expectedRecipes));
    backendService.getRecipes().subscribe(recipes => {
      expect(recipes).toEqual(expectedRecipes);
    });
    expect(httpClientSpy.get.calls.count()).toBe(1);

  });

  it("should ingredients array set to empty if undefined", () => {
    const expectedRecipes = [
     {id: 0, name: 'TestRecipe', description: 'DescriptionTest', imagePath: 'http://image.url'}
    ];

    httpClientSpy.get.and.returnValue(of(expectedRecipes));
    backendService.getRecipes().subscribe(recipes => {
      expect(recipes[0].ingredients).toBeDefined();
    });
    expect(httpClientSpy.get.calls.count()).toBe(1);
  });

  it('should return response when storing recipe', () => {
    const recipes: Recipe[] = [
      new Recipe(0, 'TestRecipe', 'DescriptionTest', 'http://image.url', [
        new Ingredient("testing", 2),
        new Ingredient("TestBread", 4)
      ]),
      new Recipe(1, 'TestRecipe2', 'DescriptionTest2', 'http://image2.url', undefined)
    ];

    httpClientSpy.put.and.returnValue(of('successfuly stored'));
    backendService.saveRecipes(recipes);
    expect(httpClientSpy.put.calls.count()).toBe(1);
  });

});
