import { Recipe } from "../recipes/recipe.model";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class BackendService {
  constructor(private httpClient: HttpClient) {}

  saveRecipes(recipes: Recipe[]): Observable<Object> {
    return this.httpClient.put(
      "https://udemy-course-aaabb.firebaseio.com/recipes.json",
      recipes,
      {
        observe: "response"
      }
    );
  }

  getRecipes() {
    return this.httpClient
      .get<Recipe[]>(
        "https://udemy-course-aaabb.firebaseio.com/recipes.json",
        {}
      )
      .map(recipes => {
        for (let recipe of recipes) {
          if (!recipe["ingredients"]) {
            recipe.ingredients = [];
          }
        }
        return recipes;
      });
  }
}
