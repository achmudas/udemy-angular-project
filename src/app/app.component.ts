import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'udemy-angular-project';

  ngOnInit() {
    firebase.initializeApp({
      apiKey: "AIzaSyA-SDcCo6oQ8473HYNWsq_ZRW5PyLTZT5A",
      authDomain: "udemy-course-aaabb.firebaseapp.com",
    });
  }

}
